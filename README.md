# Agility: Un Robot En Équilibre
### Concepteur: Jean-Michel Harvey-Perron
#### Consultant en mathématique avanc�es: daddy Abdelaziz Akili 

## Matériel:
1. Processeur - Raspberry Pi 3B+ SOC: https://static.raspberrypi.org/files/product-briefs/Raspberry-Pi-Model-Bplus-Product-Brief.pdf
2. Shield de puissance - Rpi Motor Driver Board: https://www.waveshare.com/wiki/RPi_Motor_Driver_Board#Introduction
3. Acceléro�tre/gyroscop MPU6050: https://www.invensense.com/wp-content/uploads/2015/02/MPU-6000-Datasheet1.pdf
4. Senseur ultrasonique HC-SR04: https://www.electroschematics.com/wp-content/uploads/2013/07/HC-SR04-datasheet-version-2.pdf
5. Structure en aluminium: Ancien boitier de PC

## Langage de programmation: 
1. Python3

## Principales librairies utilisées:

1. simple_pid                https://pypi.org/project/simple-pid/
2. adafruit_MPU6050:         https://github.com/adafruit/Adafruit_MPU6050

## Ressources:

1. Calibration de gyroscope: https://nxttime.wordpress.com/2010/11/03/gyro-offset-and-drift/
