#########################################################
# Un projet en Python avec un Raspberry Pi				#
# Par Abdelaziz Akili et Jean-Michel Harvey-Perron		#
# Cours: Systemes d'Exploitation Alternatifs	     	#
# Professeur: Julien Le Roux							#
# Remis le 11 Decembre 2019 au cégep de Saint-Laurent   #
#########################################################

#!/usr/bin/python3

from simple_pid import PID
import adafruit_mpu6050
import RPi.GPIO as GPIO                                                 
import subprocess
import statistics
import numpy
import busio
import board
import math
import time
import csv
import sys
import os 

screen_width = 100
calibrationCycles = 300
animation_duration = 0.1

TURNRATE = 15
FALLNOT = 23.71
BACKFALL = FALLNOT - 0.15
FRONTFALL = FALLNOT + 0.13

class shellOutput:

	def __init__(self):

		pass
	
	def flush(self):

		sys.stdout.write('\x1b[1A')
		sys.stdout.write('\x1b[2K')

shell = shellOutput()
class ultraSonic:
	
	def __init__(self,ECHO,TRIGGER):

		self.TRIGGER = TRIGGER
		self.ECHO = ECHO
		GPIO.setup(self.TRIGGER, GPIO.OUT)
		GPIO.setup(self.ECHO, GPIO.IN)
		
	def getDist(self):

		GPIO.output(self.TRIGGER, True)
 	
		time.sleep(0.00001)
		GPIO.output(self.TRIGGER, False)
 	
		StartTime = time.time()
		StopTime = time.time()
 	
		while GPIO.input(self.ECHO) == 0:
			StartTime = time.time()
 	
		while GPIO.input(self.ECHO) == 1:
			StopTime = time.time()
 	
		TimeElapsed = StopTime - StartTime
		distance = (TimeElapsed * 34300) / 2
	
		time.sleep(0.01)
 	
		return distance

class microElectroMechanicalGyroscope:

	def __init__(self):

		self.i2c = busio.I2C(board.SCL, board.SDA)
		self.mpu = adafruit_mpu6050.MPU6050(self.i2c)

		self.total_x = 0
		self.total_y = 0
		self.total_z = 0

		os.system("clear")
		time.sleep(1 * animation_duration)
		for i in range(29):
			print("")
		print("   Future-Algorithm Presents:")
		time.sleep(1* animation_duration)
		print("         ",end = '')
		os.system("figlet Agility")
		time.sleep(3* animation_duration)
		print("")
		print("   Loading Self balancing ressources..")
		print("")
		time.sleep(3* animation_duration)
		shell.flush()
		shell.flush()
		print("   Calibrating gyro:".format(i,calibrationCycles))
		print("")
		time.sleep(2.5* animation_duration)

		shell.flush()
		shell.flush()

		for i in range(0,calibrationCycles,1):

			print("   Calibrating gyro: {} / {}".format(i,calibrationCycles))
			print("")

			shell.flush()
			shell.flush()

			self.total_x += self.mpu.gyro[0]
			self.total_y += self.mpu.gyro[1]
			self.total_z += self.mpu.gyro[2]

		self.rolOffset = self.total_x / calibrationCycles
		self.pitOffset = self.total_y / calibrationCycles
		self.yawOffset = self.total_z / calibrationCycles
		time.sleep(1 * animation_duration)
		print("	Calibration done.")
		print("")
		print("")
		time.sleep(2* animation_duration)

	def getTemp(self):
		return mpu.temperature
	def getRoll(self):
		return self.mpu.gyro[0] - self.rolOffset
	def getPitch(self):
		return self.mpu.gyro[1] - self.pitOffset
	def getYaw(self):
		return self.mpu.gyro[2] - self.yawOffset

class microElectroMechanicalAccelerometer:

	def __init__(self):

		self.i2c = busio.I2C(board.SCL, board.SDA)
		self.mpu = adafruit_mpu6050.MPU6050(self.i2c)

	def getX(self):
		return self.mpu.acceleration[0]
	def getY(self):
		return self.mpu.acceleration[1]
	def getZ(self):
		return self.mpu.acceleration[2]

class electricMotor:

	def __init__(self,anode,cathode,enable):

		self.anode = anode
		self.cathode = cathode		
		self.enable = enable

		GPIO.setmode(GPIO.BCM)                                                  
		GPIO.setwarnings(False)                                                 
		GPIO.setup(self.anode,GPIO.OUT)                                              
		GPIO.setup(self.cathode,GPIO.OUT)                                              
		GPIO.setup(self.enable,GPIO.OUT)                                                 
	
		self.controller = GPIO.PWM(self.enable,500)
	
		self.dutyCycle = 0	
		self.controller.start(self.dutyCycle)

	def run(self,dutyCycle):
	
		self.dutyCycle = dutyCycle
		if dutyCycle >= -100 and dutyCycle <= 100:
	
			if dutyCycle >= 0:
		
				GPIO.output(self.anode,1)
				GPIO.output(self.cathode,0)
		
				self.controller.ChangeDutyCycle(dutyCycle)
		
			elif dutyCycle < 0:
		
				GPIO.output(self.anode,0)
				GPIO.output(self.cathode,1)
		
				self.controller.ChangeDutyCycle(-1 * dutyCycle)

class xBeeController:

	def __init__(self,b1,b2,b3,b4):

		self.Boutons = [b1,b2,b3,b4]

		GPIO.setmode(GPIO.BCM)                                                  
		GPIO.setwarnings(False)                                                 
	
		for w in range(0,4):
			GPIO.setup(self.Boutons[w],GPIO.IN)

	def getButtons(self):
		
		for x in range(0,4):
			if GPIO.input(self.Boutons[x]) == 1:
				return x
		return 4 

	def debug(self):
	
		print(self.getButtons())	
	
class balancingRobot:

	def __init__(self):
	
		#self.gyro = microElectroMechanicalGyroscope()
		#self.fileName = "data/agility_log_" + input("Name log file: ") + ".csv"
		#self.csvfile = open(self.fileName, "w+")
		self.accelero = microElectroMechanicalAccelerometer()
		self.motor_L = electricMotor(6, 13, 12)
		self.motor_R = electricMotor(21, 20, 26)
		self.sonic = ultraSonic(14,15)
		self.pid = PID(54, 111, 0.8, setpoint = FALLNOT)
		self.pid.sample_time = 0
		self.xbeeControl = xBeeController(5,7,19,16)
		
	def record(self):

		self.data = [time.time(),self.accelero.getZ(),self.accelero.getX(),self.gyro.getPitch(),self.motor_L.dutyCycle]	
		self.csvwriter = csv.writer(self.csvfile, delimiter=',')
		self.csvwriter.writerow(self.data)
	
	def visualize(self):

		print("")
		print("Agility >>>>>>",end = '')
		inst_Accel = self.accelero.getX()	
		instDist = self.sonic.getDist()	
		distVar = (screen_width / 2) - (instDist * 4) + 22
		accVar = (screen_width / 2) - int((screen_width/20) * inst_Accel)
		for i in range(0,screen_width,1):
			if(i == screen_width / 2):
				print("X",end = '')
			elif(i == accVar):
				print("a",end = '')
			elif(i == distVar):
				print("u",end = '')
			else:
				print(" ",end = '')
		print("<<<<<< ",end = '')
		print(round(inst_Accel,1),end = '')
		print(" m/s\u00b2",end = '  ')
		print(round(instDist,1),end = '')
		print(" cm")
		time.sleep(0.07)
		shell.flush()

	def Be(self):
		
		cmd = self.xbeeControl.getButtons()

		if cmd == 4:
			self.pid.setpoint = FALLNOT
			correction = -1 * self.pid(self.sonic.getDist())
			self.motor_L.run(correction)		
			self.motor_R.run(correction)
		elif cmd == 1:
			self.pid.setpoint = FRONTFALL
			correction = -1 * self.pid(self.sonic.getDist())
			self.motor_L.run(correction)		
			self.motor_R.run(correction)
		elif cmd == 3:
			self.pid.setpoint = BACKFALL
			correction = -1 * self.pid(self.sonic.getDist())
			self.motor_L.run(correction)		
			self.motor_R.run(correction)
		elif cmd == 2:
			correction = -1 * self.pid(self.sonic.getDist())
			self.motor_L.run(correction + TURNRATE)		
			self.motor_R.run(correction - TURNRATE)
		elif cmd == 0:
			correction = -1 * self.pid(self.sonic.getDist())
			self.motor_L.run(correction - TURNRATE)		
			self.motor_R.run(correction + TURNRATE)

##############################################
# # # # # # # # # # Main # # # # # # # # # # #
##############################################
agility = balancingRobot()
while True:
	agility.Be()
