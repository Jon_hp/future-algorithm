############################################
# This Script takes the name of a CSV-File #
# as parameter and plots A Graph           #
# by John HP                               #
# Thu Nov 28 00:10:48 EST 201              #
############################################
#!/usr/bin/python3
from matplotlib import pyplot as plt
import sys

csv_line = []
tim_list = []
az_list = []
ax_list = []
gx_list = []
pwm_list = []

csv_file = open("{}".format(sys.argv[1]), 'r')

for line in csv_file:

	csv_line.append(line.strip().split(','))

for index in range(1,len(csv_line)):

	tim_list.append(1000 * (float(csv_line[index][0])-float(csv_line[1][0])))
	az_list.append(float(csv_line[index][1]))
	ax_list.append(float(csv_line[index][2])*10)
	gx_list.append(float(csv_line[index][3]))
	pwm_list.append(float(csv_line[index][4]))

plt.plot(tim_list,ax_list,label = 'Accel_X * 10')
plt.plot(tim_list,pwm_list,label = 'pwm')
#plt.plot(tim_list,az_list,label = 'Accel_Z')
#plt.plot(tim_list,gx_list,label = 'Gyro_x')

plt.grid()
plt.xlabel('Time(ms)')
plt.ylabel('Values')
plt.legend()

plt.show()
